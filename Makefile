PREFIX = /usr

all:
	@echo Run \'make install\' to install pbf.

install:
	@mkdir -p $(DESTDIR)$(PREFIX)/bin
	@cp -p pbf.sh $(DESTDIR)$(PREFIX)/bin/pbf
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/pbf
	@cp -p conv.py $(DESTDIR)$(PREFIX)/bin/pbfpy
	@chmod 755 $(DESTDIR)$(PREFIX)/bin/pbfpy


uninstall:
	@rm -rf $(DESTDIR)$(PREFIX)/bin/pbf
	@rm -rf $(DESTDIR)$(PREFIX)/bin/pbfpy
